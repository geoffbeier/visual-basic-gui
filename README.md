# A GUI Interface using Visual BASIC

I usually make something dumb when I learn a new toolkit. Today, it's [a GUI interface using Visual BASIC](https://www.youtube.com/watch?v=hkDD03yeLnU), to track a killer's IP address.

I can't imagine this is useful for anyone at all. In the unlikely event it is, enjoy it under an MIT-like license.

The video component is copied from [this svelte youtube component](https://github.com/sveltecasts/svelte-youtube) with a few modifications to enable autoplay on browsers that support it. I think we can all agree that autoplay, while evil, is justified in this instance.

CSS is from [Jordan Scales' beautiful project](https://github.com/jdan/98.css).

The `.gitlab-ci.yml` file in this repository causes gitlab to process any commits and host them automatically on [geoffbeier.gitlab.io](https://geoffbeier.gitlab.io/visual-basic-gui/) for the world to see. 


## Get started

Install the dependencies...

```bash
cd svelte-app
npm install
```

...then start [Rollup](https://rollupjs.org):

```bash
npm run dev
```

Navigate to [localhost:5000](http://localhost:5000). You should see your app running. Edit a component file in `src`, save it, and reload the page to see your changes.

By default, the server will only respond to requests from localhost. To allow connections from other computers, edit the `sirv` commands in package.json to include the option `--host 0.0.0.0`.


## Building and running in production mode

To create an optimised version of the app:

```bash
npm run build
```

You can run the newly built app with `npm run start`. This uses [sirv](https://github.com/lukeed/sirv), which is included in your package.json's `dependencies` so that the app will work when you deploy to platforms like [Heroku](https://heroku.com).


## Single-page app mode

By default, sirv will only respond to requests that match files in `public`. This is to maximise compatibility with static fileservers, allowing you to deploy your app anywhere.

If you're building a single-page app (SPA) with multiple routes, sirv needs to be able to respond to requests for *any* path. You can make it so by editing the `"start"` command in package.json:

```js
"start": "sirv public --single"
```


## Deploying to the web

### With [now](https://zeit.co/now)

Install `now` if you haven't already:

```bash
npm install -g now
```

Then, from within your project folder:

```bash
cd public
now deploy --name my-project
```

As an alternative, use the [Now desktop client](https://zeit.co/download) and simply drag the unzipped project folder to the taskbar icon.

### With [surge](https://surge.sh/)

Install `surge` if you haven't already:

```bash
npm install -g surge
```

Then, from within your project folder:

```bash
npm run build
surge public my-project.surge.sh
```
